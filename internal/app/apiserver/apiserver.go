package apiserver

import (
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"http-rest-api/internal/store"
	"io"
	"net/http"
)

type APIServer struct {
	serverconfig *Config
	logger       *logrus.Logger
	router       *mux.Router
	store        *store.Store
}

func New(config *Config) *APIServer {
	return &APIServer{
		serverconfig: config,
		logger:       logrus.New(),
		router:       mux.NewRouter(),
	}
}

func (s *APIServer) Start() error {

	if err := s.configureLogger(); err != nil {
		return err
	}

	s.configureLogger()
	s.configureRouter()

	if err := s.configureStore(); err != nil {
		return err
	}

	s.logger.Info("starting API server...")

	return http.ListenAndServe(s.serverconfig.BinAddr, s.router)
}

func (s *APIServer) configureLogger() error {
	level, err := logrus.ParseLevel(s.serverconfig.LogLevel)

	if err != nil {
		return err
	}

	s.logger.SetLevel(level)
	return nil
}

func (s *APIServer) configureRouter() {
	s.router.HandleFunc("/hello", s.handleHello())
}

func (s *APIServer) handleHello() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		io.WriteString(w, "Hello")
	}
}

func (s *APIServer) configureStore() error {
	st := store.New(s.serverconfig.Store)
	if err := st.Open(); err != nil {
		return err
	}

	s.store = st
	return nil
}
