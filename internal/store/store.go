package store

import (
	"database/sql"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/lib/pq"
)

type Store struct {
	config         *Config
	db             *sql.DB
	userRepository *UserRepository
}

func New(conf *Config) *Store {
	return &Store{
		config: conf,
	}
}

func (s *Store) Open() error {
	chckdb, err := sql.Open("postgres", s.config.DatabaseURL)

	if err != nil {
		return nil
	}

	if err := chckdb.Ping(); err != nil {
		return err
	}

	s.db = chckdb
	return nil
}

func (s *Store) Close() {

}

func (s *Store) User() *UserRepository {
	if s.userRepository != nil {
		return s.userRepository
	}

	s.userRepository = &UserRepository{store: s}

	return s.userRepository
}
